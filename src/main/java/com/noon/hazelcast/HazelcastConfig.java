package com.noon.hazelcast;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * Created by hareesh.pammi on 2019-10-14
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HazelcastConfig {
    private String name;

    private String password;

    private int networkPort;

    private String zookeeperUrl;

    private String zookeeperPath;

    private String zookeeperGroup;

    private Map<String, HazelcastMapStoreConfig> mapStoreConfigs;
}
