package com.noon.hazelcast;

import com.hazelcast.config.*;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.spi.properties.GroupProperty;
import com.hazelcast.zookeeper.ZookeeperDiscoveryProperties;
import com.hazelcast.zookeeper.ZookeeperDiscoveryStrategyFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by hareesh.pammi on 2019-10-14
 */
public class HazelcastInstanceFactory {
    private static volatile HazelcastInstance hazelcastInstance;

    private HazelcastInstanceFactory(HazelcastConfig hazelcastConfig) {
        this.hazelcastInstance = Hazelcast.newHazelcastInstance(getConfigObject(hazelcastConfig));
    }

    public static HazelcastInstance getInstance(HazelcastConfig hazelcastConfig) {
        if (hazelcastInstance == null) {
            synchronized (HazelcastInstanceFactory.class) {
                if (hazelcastInstance == null) {
                    new HazelcastInstanceFactory(hazelcastConfig);
                }
            }
        }

        return hazelcastInstance;
    }

    public Config getConfigObject(HazelcastConfig hazelcastConfig) {
        Config config = new Config();
        config.setGroupConfig(new GroupConfig(hazelcastConfig.getName(), hazelcastConfig.getPassword()));
        config.setProperty(GroupProperty.DISCOVERY_SPI_ENABLED.getName(), "true");

        NetworkConfig networkConfig = new NetworkConfig();
        networkConfig.setPort(hazelcastConfig.getNetworkPort());
        networkConfig.getJoin().getMulticastConfig().setEnabled(false);
        DiscoveryStrategyConfig discoveryStrategyConfig = new DiscoveryStrategyConfig(new ZookeeperDiscoveryStrategyFactory());
        discoveryStrategyConfig.addProperty(ZookeeperDiscoveryProperties.ZOOKEEPER_URL.key(), hazelcastConfig.getZookeeperUrl());
        discoveryStrategyConfig.addProperty(ZookeeperDiscoveryProperties.ZOOKEEPER_PATH.key(), hazelcastConfig.getZookeeperPath());
        discoveryStrategyConfig.addProperty(ZookeeperDiscoveryProperties.GROUP.key(), hazelcastConfig.getZookeeperGroup());
        networkConfig.getJoin().getDiscoveryConfig().addDiscoveryStrategyConfig(discoveryStrategyConfig);

        config.setNetworkConfig(networkConfig);

        Map<String, MapConfig> mapConfigsMap = new HashMap<>();
        hazelcastConfig.getMapStoreConfigs().keySet().forEach(key -> {
            MapConfig mapConfig = new MapConfig();
            HazelcastMapStoreConfig hzMapStoreConfig = hazelcastConfig.getMapStoreConfigs().get(key);

            mapConfig.setBackupCount(hzMapStoreConfig.getBackupCount());
            mapConfig.setMaxIdleSeconds(hzMapStoreConfig.getMaxIdleSeconds());
            mapConfig.setTimeToLiveSeconds(hzMapStoreConfig.getTimeToLiveSeconds());
            mapConfig.setEvictionPolicy(hzMapStoreConfig.getEvictionPolicy());

            MapStoreConfig mapStoreConfig = new MapStoreConfig();
            mapStoreConfig.setImplementation(hzMapStoreConfig.getMapStoreImplementationObject());
            mapConfig.setMapStoreConfig(mapStoreConfig);

            MaxSizeConfig maxSizeConfig = new MaxSizeConfig();
            maxSizeConfig.setSize(hzMapStoreConfig.getSize());
            mapConfig.setMaxSizeConfig(maxSizeConfig);

            if (hzMapStoreConfig.isEnableNearCache()) {
                NearCacheConfig nearCacheConfig = new NearCacheConfig();
                nearCacheConfig.setMaxSize(hzMapStoreConfig.getNearCacheSize());
                nearCacheConfig.setMaxIdleSeconds(hzMapStoreConfig.getNearCacheMaxIdleSeconds());
                nearCacheConfig.setTimeToLiveSeconds(hzMapStoreConfig.getNearCacheTimeToLiveSeconds());
                nearCacheConfig.setEvictionPolicy(hzMapStoreConfig.getNearCacheEvictionPolicy().name());

                mapConfig.setNearCacheConfig(nearCacheConfig);
            }

            mapConfigsMap.put(key, mapConfig);
        });

        config.setMapConfigs(mapConfigsMap);

        return config;
    }
}
