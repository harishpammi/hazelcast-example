package com.noon.hazelcast;

import com.hazelcast.config.EvictionPolicy;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by hareesh.pammi on 2019-10-14
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class HazelcastMapStoreConfig {
    private int backupCount = 1;

    private int timeToLiveSeconds;

    private int maxIdleSeconds;

    private int size;

    private EvictionPolicy evictionPolicy;

    private boolean enableNearCache;

    private int nearCacheTimeToLiveSeconds;

    private int nearCacheMaxIdleSeconds;

    private int nearCacheSize;

    private EvictionPolicy nearCacheEvictionPolicy;

    private Object mapStoreImplementationObject;
}
