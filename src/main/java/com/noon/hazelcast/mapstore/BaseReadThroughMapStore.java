package com.noon.hazelcast.mapstore;

import com.hazelcast.core.MapStore;

import java.util.Collection;
import java.util.Map;

/**
 * Created by hareesh.pammi on 2019-10-18
 */
public abstract class BaseReadThroughMapStore<K,V> implements MapStore<K, V> {
    @Override
    public void store(K k, V v) {

    }

    @Override
    public void storeAll(Map<K, V> map) {

    }

    @Override
    public void delete(K k) {

    }

    @Override
    public void deleteAll(Collection<K> collection) {

    }

    @Override
    public abstract V load(K k);

    @Override
    public abstract Map<K, V> loadAll(Collection<K> collection);

    @Override
    public Iterable<K> loadAllKeys() {
        return null;
    }
}
