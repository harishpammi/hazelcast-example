package com.noon.hazelcasttest;

import com.hazelcast.config.EvictionPolicy;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.noon.hazelcast.HazelcastConfig;
import com.noon.hazelcast.HazelcastInstanceFactory;
import com.noon.hazelcast.HazelcastMapStoreConfig;
import com.noon.mapstore.TestMap1MapStore;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * Created by hareesh.pammi on 2019-10-15
 */
public class HzTest {
    public static void main(String[] args) {
        TestMap1MapStore testMap1MapStore = new TestMap1MapStore();
        HazelcastMapStoreConfig hazelcastMapStoreConfig1 = new HazelcastMapStoreConfig(1, 2, 30, 2, EvictionPolicy.LRU, false, 10, 10, 10, EvictionPolicy.LRU, testMap1MapStore);
        Map<String, HazelcastMapStoreConfig> mapStoreConfigMap = new HashMap<>();
        mapStoreConfigMap.put("testMap1", hazelcastMapStoreConfig1);

        HazelcastConfig hazelcastConfig = new HazelcastConfig("test", "test", 5701, "127.0.0.1:2181", "/testHzZk", "Test", mapStoreConfigMap);


        HazelcastInstance hazelcastInstance = HazelcastInstanceFactory.getInstance(hazelcastConfig);
        IMap<String, String> testMap1Cache = hazelcastInstance.getMap("testMap1");
        System.out.println(testMap1Cache.keySet().size());

        System.out.println(testMap1Cache.get("M"));
        System.out.println(testMap1Cache.keySet().size());

        System.out.println(testMap1Cache.get("B"));
        System.out.println(testMap1Cache.keySet().size());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(testMap1Cache.getAll(new HashSet<>(Arrays.asList("M", "N", "C"))));
        System.out.println(testMap1Cache.keySet().size());

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(testMap1Cache.keySet().size());
        System.out.println(testMap1Cache.keySet());
    }
}
