package com.noon.mapstore;

import com.noon.hazelcast.mapstore.BaseReadThroughMapStore;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by hareesh.pammi on 2019-10-15
 */
public class TestMap1MapStore extends BaseReadThroughMapStore<String, String> {
    private Map<String, String> testData = new HashMap<>();

    public TestMap1MapStore() {
        testData.put("A", "testMap1A");
        testData.put("B", "testMap1A");
        testData.put("C", "testMap1A");
        testData.put("D", "testMap1A");
        testData.put("E", "testMap1A");
        testData.put("F", "testMap1A");
        testData.put("G", "testMap1A");
        testData.put("H", "testMap1A");
        testData.put("I", "testMap1A");
        testData.put("J", "testMap1A");
        testData.put("K", "testMap1A");
        testData.put("L", "testMap1A");
        testData.put("M", "testMap1A");
        testData.put("N", "testMap1A");
        testData.put("O", "testMap1A");

    }

    @Override
    public String load(String s) {
        System.out.println("Loading: " + s);
        return testData.get(s);
    }

    @Override
    public Map<String, String> loadAll(Collection<String> collection) {
        Map<String, String> loadAllMap = new HashMap<>();
        collection.stream().filter(s -> testData.containsKey(s)).forEach(str -> {
            System.out.println("Loading: " + str);
            loadAllMap.put(str, testData.get(str));
        });
        return loadAllMap;
    }
}
